export let RedrawType = {
    up: "up",
    down: "down"
}

export class A4Paper{
    constructor(a4_paper_width, a4_paper_height, a4_paper_x, a4_paper_y){
        this._a4_paper_width = a4_paper_width;
        this._a4_paper_height = a4_paper_height;
        this._a4_paper_x = a4_paper_x;
        this._a4_paper_y = a4_paper_y;
        this._initialize_a4_paper = false;

        this._canvas_context = null;

        this._old_a4_paper_x = 0;
        this._old_a4_paper_y = 0;
    }

    draw_a4_paper(){
        this.draw_a4_paper_border();

        this._canvas_context.fillStyle = "rgb(254, 254, 254)";
        this._canvas_context.fillRect(this._a4_paper_x, this._a4_paper_y, this._a4_paper_width,
            this._a4_paper_height);

        if(!this._initialize_a4_paper){
            this._initialize_a4_paper = true;
        }
        this._old_a4_paper_x = this._a4_paper_x;
        this._old_a4_paper_y = this._a4_paper_y;
    }

    draw_a4_paper_border(){
        this._canvas_context.fillStyle = "rgb(160, 160, 160)";
        this._canvas_context.fillRect(this._a4_paper_x - 1, this._a4_paper_y - 1, this._a4_paper_width + 1, 1);
        this._canvas_context.fillRect(this._a4_paper_x + this._a4_paper_width, this._a4_paper_y - 1, 1, this._a4_paper_height + 2);
        this._canvas_context.fillRect(this._a4_paper_x, this._a4_paper_y + this._a4_paper_height + 1, this._a4_paper_width + 1, 1);
        this._canvas_context.fillRect(this._a4_paper_x - 1, this._a4_paper_y, 1, this._a4_paper_height + 2);
    }

    redraw_a4_paper(redraw_x, redraw_y, redraw_type){
        if(this._initialize_a4_paper){
            this._canvas_context.clearRect(this._old_a4_paper_x - 2, this._old_a4_paper_y - 1,
                this._a4_paper_width + 3, this._a4_paper_height + 3);
            
            this._a4_paper_x = redraw_x ?? this._a4_paper_x;
            if(redraw_type === RedrawType.up){
                this._a4_paper_y += redraw_y;
            }

            if(redraw_type === RedrawType.down){
                this._a4_paper_y -= redraw_y;
            }

            this.draw_a4_paper();
        }
    }

    set_context(context){
        this._canvas_context = context;
    }
}