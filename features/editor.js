import { A4Paper, RedrawType } from "./a4_paper";

export class Editor{
    constructor(canvas, canvas_width){
        this._canvas = canvas;
        this._canvas_context = this._canvas.getContext("2d");
        this._a4_paper = new A4Paper(794, 1123, canvas_width / 3.5, 20);
    }

    render(){
        this._a4_paper.set_context(this._canvas_context);
        this._a4_paper.draw_a4_paper();
        this.initialize_wheel_mouse_event();
    }

    initialize_wheel_mouse_event(){
        document.addEventListener("wheel", (wheel_event) => this.scroll_editor(wheel_event));
    }

    scroll_editor(event_wheel_scroll){
        if(event_wheel_scroll.deltaY === -100){
            this._a4_paper.redraw_a4_paper(null, 100, RedrawType.up);
        }

        if(event_wheel_scroll.deltaY === 100){
            this._a4_paper.redraw_a4_paper(null, 100, RedrawType.down);
        }
    }
}