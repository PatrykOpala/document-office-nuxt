import {writeFile} from 'node:fs'

let done = "success";

export default defineEventHandler((event)=>{
    writeFile(`dotest/test.txt`, "Nuxt Api test.", (err)=>{
        if(err){
            done = "failed"
        }
        done = "success";
    });
    return{
        odp: done
    }
});